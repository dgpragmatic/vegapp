//
//  ParametrosConstants.swift
//  vegapp
//
//  Created by diego garcia on 11/8/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

struct EndPoints {
//http://190.7.144.179:8089
   private static let  URI_BASE = "http://10.10.10.4:8080/GvIntegrator2/rest/"
   static let SINCRONIZACION_INICIAL = URI_BASE + "serviciosComunes/getSincronizacionInicial"
    
    static func sincronizacionMaestrosFinca(finca:String, modulo:Int, usuario:String) -> String{
      return URI_BASE + "serviciosComunes/getSincronizacionMaestrosFinca"+"/"+finca+"/"+String(modulo)+"/"+usuario
    }
    
    static func sincronizarTarjetas(finca:String, bloque:String ,modulo:Int) ->String {
        return URI_BASE + "serviciosProduccion/getSincronizacionTarjetasSiembra"+"/"+finca+"/"+bloque+"/"+String(modulo)
    }
}


struct Modulo {
    static let TIPO_MODULO_PRODUCCION = 1
    static let TIPO_MODULO_PROPAGACION = 2
}
