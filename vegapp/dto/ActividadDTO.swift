//
//  ActividadDTO.swift
//  vegapp
//
//  Created by diego garcia on 11/16/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class ActividadDTO{
    
    private var codActi:String?
    private var nomActi:String?
    
    func getCodActi()->String{
        return codActi!
    }
    
    func getNomActi()->String{
        return nomActi!
    }
    
    func setCodActi(codActi:String){
        self.codActi = codActi
    }
    
    func setNomActi(nomActi:String){
        self.nomActi = nomActi
    }
    
    
}
