//
//  FincaDTO.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

 class FincaDTO{
    
    private var codFinca:String
    private var nomFinca:String
    
    init(codFinca:String,nomFinca:String){
        self.codFinca = codFinca
        self.nomFinca = nomFinca
    }
    
    init() {
        codFinca = ""
        nomFinca = ""
    }
    
    
    
    public func getCodFinca() -> String{
        return codFinca
    }
    
    public func getNomFinca() -> String{
        return nomFinca
    }
    
    public func setNomFinca(nomFinca:String){
        self.nomFinca = nomFinca
    }
    
    public func setCodFinca(codFinca:String){
        self.codFinca = codFinca
    }
   
    
}
