//
//  TarjetaSiembraDTO.swift
//  vegapp
//
//  Created by diego garcia on 11/16/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class TarjetaSiembraDTO{
    
    private var camaDTO:CamaDTO?
    private var actividadDTO:ActividadDTO?
    private var fechaPrograma:String?;
    private var diasPrograma:Int?;
    private var observacion:String?;
    private var fechaSincronizacion:String?;
    private var id:String?
    
    
    init(){
    
    }
    
    func getCamaDTO() -> CamaDTO{
        if camaDTO == nil {
            camaDTO = CamaDTO()
        }
        return camaDTO!
    }
    
    func setCamaDTO(camaDTO:CamaDTO){
        self.camaDTO = camaDTO
    }
    
    func getActividadDTO() -> ActividadDTO{
        if actividadDTO == nil {
            actividadDTO = ActividadDTO()
        }
        return actividadDTO!
    }
    
    func setActividadDTO(actividadDTO:ActividadDTO){
        self.actividadDTO = ActividadDTO()
    }
    
    func  getFechaPrograma()->String{
        return  fechaPrograma!
    }
    
    func setFechaPrograma(fechaPrograma:String){
       self.fechaPrograma = fechaPrograma
    }
    
    func getDiasPrograma()->Int {
        return diasPrograma!
    }
    
    func setDiasPrograma(diasPrograma:Int){
        self.diasPrograma = diasPrograma
    }
    
    func getObservacion() ->String {
        return observacion!
    }
    
    func setObservacion(observacion:String){
        self.observacion = observacion
    }
    
    func setId(id:String){
        self.id = id
    }
    
    func getId()->String{
        return id!
    }
    
    
    func getFechaSincronizacion()->String{
        return fechaSincronizacion!
    }
    
    func setFechaSincronizacion(fechaSincronizacion:String) {
        self.fechaSincronizacion = fechaSincronizacion
    }
    
}
