//
//  ProductoDTO.swift
//  vegapp
//
//  Created by diego garcia on 11/16/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class ProductoDTO{
    
    private var  codPro:String?
    private var  nomPro:String?
    private var gradoDTO:GradoDTO?
    
    init(){
        
    }

    func getCodPro() -> String{
        return codPro!
    }
    
    func getNomPro() -> String{
        return nomPro!
    }
    
    func setcodPro(codPro:String){
        self.codPro = codPro
    }
    
    func setNomPro(nomPro:String){
        self.nomPro = nomPro
    }
    
    
}
