//
//  GradoDTO.swift
//  vegapp
//
//  Created by diego garcia on 11/16/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class GradoDTO{
    
    private var codGrado:String?
    private var nomGrado:String?
    private var nomAbGrado:String?
    private var tipoGrado:String?
    private var codPro:String?
    
    init(){
        
    }
    
    func setCodGrado(codGrado:String){
        self.codGrado = codGrado
    }
    
    func getCodGrado() ->String{
        return codGrado!
    }
    
    func setNomGrado(nomGrado:String){
        self.nomGrado = nomGrado
    }
    
    func getNomGrado() ->String{
        return nomGrado!
    }
    
    func setNomAbGrado(nomAbGrado:String){
        self.nomAbGrado = nomAbGrado
    }
    
    func getNomAbGrado() ->String{
        return nomAbGrado!
    }
    
    func setCodPro(codPro:String){
        self.codPro = codPro
    }
    
    func getCodPro() ->String{
        return codPro!
    }
    
    
}
