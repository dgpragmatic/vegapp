//
//  SincronizacionInvernaderoDTO.swift
//  vegapp
//
//  Created by diego garcia on 11/14/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class SincronizacionInvernaderoDTO : JSONUtil{
    private var invernaderos:[InvernaderoDTO] = []
    
    
    init(jsonArray:[[String: Any]]){
        super.init()
        var invernaderoDTO:InvernaderoDTO?
        for invernadero in jsonArray{
                invernaderoDTO = InvernaderoDTO()
                let finca = getJsonObject(jsonObject: invernadero, forKey: "fincaDTO")
                let codFinca = getStringJsonValue(jsonObject: finca!, forKey: "codFinca")
                invernaderoDTO?.getFincaDTO().setCodFinca(codFinca: codFinca!)
                let modulo = getJsonObject(jsonArray: invernadero,forKey:"moduloDTO")
                let codModulo = getIntJsonValue(jsonObject:modulo!,forKey:"codModulo")
                invernaderoDTO?.getModuloDTO().setCodModulo(codModulo: codModulo!)
                let nomBloque = getStringJsonValue(jsonObject:invernadero,forKey:"nomBloque")
                invernaderoDTO?.setNomBloque(nomBloque: nomBloque!)
                let codBloque = getStringJsonValue(jsonObject: invernadero, forKey: "codBloque")
                invernaderoDTO?.setCodBloque(codBloque: codBloque!)
                invernaderos.append(invernaderoDTO!)
            }
    }
    
    func getInvernaderos() -> [InvernaderoDTO]{
        return invernaderos
    }
}
