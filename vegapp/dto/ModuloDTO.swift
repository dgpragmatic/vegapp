//
//  ModuloDTO.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class ModuloDTO{
    private var codModulo:Int?;
    private var nomModulo:String?;
    
    init(codModulo:Int,nomModulo:String) {
       self.codModulo = codModulo
       self.nomModulo = nomModulo
    }
    
    init(){}
    
    func getCodModulo()-> Int{
        return codModulo!
    }
    
    
    func setNomModulo(nomModulo:String){
        self.nomModulo = nomModulo
    }
    
    func getNomModulo()-> String {
       return nomModulo!
    }
    
    func setCodModulo(codModulo:Int){
        self.codModulo = codModulo
    }
    
    
    
    
}
