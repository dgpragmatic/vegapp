//
//  CamaDTO.swift
//  vegapp
//
//  Created by diego garcia on 11/16/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class CamaDTO{
    
    private var id:String
    private var invernaderoDTO:InvernaderoDTO?
    private var cama:Int
    private var nroCama:String
    private var nroNave:Int
    private var areaCama:Double
    private var plantasCama:Int
    private var fechaSiem:String?
    private var variedadDTO:VariedadDTO?
    private var mercadoDTO:MercadoDTO?
    private var periodoProyeccion:String
    private var fechaPlaneada:String
    private var fechaPicoCorte:String
    private var diaPicoCorte:Int
    
    init(){
        id = ""
        cama = 0
        nroCama = ""
        nroNave = 0
        areaCama = 0
        plantasCama = 0
        periodoProyeccion = ""
        fechaPlaneada = ""
        fechaPicoCorte = ""
        diaPicoCorte = 0
        
    }
    
    func getId()-> String{
        return id
    }
    
    func setId(id:String){
        self.id = id
    }
    
    func getInvernaderoDTO() -> InvernaderoDTO {
        if(invernaderoDTO == nil){
            invernaderoDTO = InvernaderoDTO()
        }
        return invernaderoDTO!
    }
    
    func setInvernaderoDTO(invernaderoDTO:InvernaderoDTO){
        self.invernaderoDTO = invernaderoDTO
    }
    
    func getCama()->Int{
        return cama
    }
    
    func setCama(cama:Int){
        self.cama = cama
    }
    
    func setNroCama(nroCama:String){
        self.nroCama = nroCama
    }
    
    func getNroCama()->String{
        return nroCama
    }
    
    func getNroNave()->Int{
        return nroNave
    }
    
    func setNroNave(nroNave:Int){
        self.nroNave = nroNave
    }
    
    func getAreaCama()->Double{
        return areaCama
    }
    
    func setAreaCama(areaCama:Double){
        self.areaCama = areaCama
    }
    
    func getPlantasCama()->Int{
        return plantasCama
    }
    
    func setPlantasCama(plantasCama:Int){
        self.plantasCama = plantasCama
    }
    
    func getFechaSiem()->String{
        return fechaSiem!
    }
    
    func setFechaSiem(fechaSiem:String){
        self.fechaSiem = fechaSiem
    }
    
    func getFechaPicoCorte()->String{
        return fechaPicoCorte
    }
    
    
    func getVariedadDTO()->VariedadDTO{
        if variedadDTO == nil{
            variedadDTO = VariedadDTO()
        }
        return variedadDTO!
    }
    
    func setVariedadDTO(variedadDTO:VariedadDTO){
        self.variedadDTO = variedadDTO
    }
    
    func getMercadoDTO() -> MercadoDTO{
        if mercadoDTO == nil{
            mercadoDTO = MercadoDTO()
        }
        return mercadoDTO!
    }
    
    func setMercadoDTO(mercadoDTO:MercadoDTO){
        self.mercadoDTO = mercadoDTO
    }
    
    func getPeriodoProyeccion() ->String{
        return periodoProyeccion
    }
    
    func setPeriodoProyeccion(periodoProyeccion:String){
        self.periodoProyeccion = periodoProyeccion
    }
    
    func getfechaPlaneada() ->String{
        return fechaPlaneada
    }
    
    func setFechaPlaneada(fechaPlaneada:String){
        self.fechaPlaneada = fechaPlaneada
    }
    
    func getfechaPicoCorte()->String{
        return fechaPicoCorte
    }
    
    func setFechaPicoCorte(fechaPicoCorte:String){
        self.fechaPicoCorte = fechaPicoCorte
    }
    
    func getDiaPicoCorte()-> Int{
        return diaPicoCorte
    }
    
    func setDiaPicoCorte(diaPicoCorte:Int){
       self.diaPicoCorte = diaPicoCorte
    }
    
}
