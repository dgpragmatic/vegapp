//
//  InvernaderoTarjetaDTO.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation
import UIKit

class InvernaderoTarjetaDTO{
    
   
    
    private var invernaderoDTO:InvernaderoDTO?;
    private var fechaSincronizacion:String;
    private var selected:Bool
    private var backGround:UIColor?
    
    init() {
        selected = false
        fechaSincronizacion = ""
    }
    
    public func getInvernaderoDTO() -> InvernaderoDTO{
        return invernaderoDTO!
    }
    
    public func setInvernaderoDTO(invernaderoDTO:InvernaderoDTO){
        self.invernaderoDTO = invernaderoDTO
    }
    
    public func getFechaSincronizacion()-> String{
        return fechaSincronizacion
    }
    
    public func isSelected()-> Bool!{
        return selected
    }
    
    public func setSelected(selected:Bool){
       self.selected = selected
    }
    
    public func setBackGroundColor(backGround:UIColor){
        self.backGround = backGround
    }
    
    public func getBackGroundColor()->UIColor{
        return backGround!
    }
    
}
