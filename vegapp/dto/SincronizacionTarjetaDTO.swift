//
//  SincronizacionTarjetaDTO.swift
//  vegapp
//
//  Created by diego garcia on 11/16/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class SincronizacionTarjetaDTO:JSONUtil{
    
    private var tarjetas:[TarjetaSiembraDTO] = []
   
    
    init(jsonObject:[String: Any]){
        super.init()
        
        let tarjetaSiembraArray = getJsonArray(jsonArray: jsonObject, forKey: "tarjetasSiembra")
        for tarjetaSiembraObj in tarjetaSiembraArray! {
            let tarjetaSiembra = TarjetaSiembraDTO()
            let camaObj =  getJsonObject(jsonObject: tarjetaSiembraObj, forKey: "camaDTO")
            let invernaderoObj = getJsonObject(jsonObject: camaObj!, forKey: "invernaderoDTO")
            let fincaObj = getJsonObject(jsonObject: invernaderoObj!, forKey: "fincaDTO")
            let codFinca = getStringJsonValue(jsonObject: fincaObj!, forKey: "codFinca")
            tarjetaSiembra.getCamaDTO().getInvernaderoDTO().getFincaDTO().setCodFinca(codFinca:codFinca!)
            let moduloObj = getJsonObject(jsonObject: invernaderoObj!, forKey: "moduloDTO")
            let codModulo = getIntJsonValue(jsonObject: moduloObj!, forKey: "codModulo")
            let codBloque = getStringJsonValue(jsonObject: invernaderoObj!, forKey: "codBloque")
            let nomBloque = getStringJsonValue(jsonObject: invernaderoObj!, forKey: "nomBloque")
            tarjetaSiembra.getCamaDTO().getInvernaderoDTO().getModuloDTO().setCodModulo(codModulo: codModulo!)
            tarjetaSiembra.getCamaDTO().getInvernaderoDTO().setCodBloque(codBloque: codBloque!)
            tarjetaSiembra.getCamaDTO().getInvernaderoDTO().setNomBloque(nomBloque: nomBloque!)
            let cama = getIntJsonValue(jsonObject: camaObj!, forKey: "cama")
            let nroCama = getStringJsonValue(jsonObject: camaObj!, forKey: "nroCama")
            let fechaSiem = getStringJsonValue(jsonObject: camaObj!, forKey: "fechaSiem")
            tarjetaSiembra.getCamaDTO().setCama(cama: cama!)
            tarjetaSiembra.getCamaDTO().setNroCama(nroCama: nroCama!)
            tarjetaSiembra.getCamaDTO().setFechaSiem(fechaSiem: fechaSiem!)
            let variedadObj = getJsonObject(jsonObject: camaObj!, forKey: "variedadDTO")
            let codVar = getStringJsonValue(jsonObject: variedadObj!, forKey: "codVar")
            let nomVar = getStringJsonValue(jsonObject: variedadObj!, forKey: "nomVar")
            tarjetaSiembra.getCamaDTO().getVariedadDTO().setCodVar(codVar: codVar!)
            tarjetaSiembra.getCamaDTO().getVariedadDTO().setNomVar(nomVar: nomVar!)
            let productoObj = getJsonObject(jsonObject: variedadObj!, forKey: "productoDTO")
            let codPro = getStringJsonValue(jsonObject: productoObj!, forKey: "codPro")
            let nomPro = getStringJsonValue(jsonObject: productoObj!, forKey: "nomPro")
            tarjetaSiembra.getCamaDTO().getVariedadDTO().getProductoDTO().setcodPro(codPro: codPro!)
            tarjetaSiembra.getCamaDTO().getVariedadDTO().getProductoDTO().setNomPro(nomPro: nomPro!)
            let mercadoObj =  getJsonObject(jsonObject: camaObj!, forKey: "mercadoDTO")
            let codMercado = getIntJsonValue(jsonObject: mercadoObj!, forKey: "codMercado")
            let nomMercado = getStringJsonValue(jsonObject: mercadoObj!, forKey: "nomMercado")
            tarjetaSiembra.getCamaDTO().getMercadoDTO().setCodMercado(codMercado: codMercado!)
            tarjetaSiembra.getCamaDTO().getMercadoDTO().setNomMercado(nomMercado: nomMercado!)
            let fechaPicoCorte = getStringJsonValue(jsonObject: camaObj!, forKey: "fechaPicoCorte")
            let diaPicoCorte = getIntJsonValue(jsonObject: camaObj!, forKey: "diaPicoCorte")
            tarjetaSiembra.getCamaDTO().setFechaPicoCorte(fechaPicoCorte: fechaPicoCorte!)
            tarjetaSiembra.getCamaDTO().setDiaPicoCorte(diaPicoCorte: diaPicoCorte!)
            let actividadObj = getJsonObject(jsonObject: tarjetaSiembraObj, forKey: "actividadDTO")
            let codActi = getStringJsonValue(jsonObject: actividadObj!, forKey: "codActi")
            let nomActi = getStringJsonValue(jsonObject: actividadObj!, forKey: "nomActi")
            tarjetaSiembra.getActividadDTO().setCodActi(codActi: codActi!)
            tarjetaSiembra.getActividadDTO().setNomActi(nomActi: nomActi!)
            let fechaPrograma = getStringJsonValue(jsonObject: tarjetaSiembraObj, forKey: "fechaPrograma")
            let diasPrograma = getIntJsonValue(jsonObject: tarjetaSiembraObj, forKey: "diasPrograma")
            let observacion = getStringJsonValue(jsonObject: tarjetaSiembraObj, forKey: "observacion")
            tarjetaSiembra.setFechaPrograma(fechaPrograma: fechaPrograma!)
            tarjetaSiembra.setDiasPrograma(diasPrograma: diasPrograma!)
            tarjetaSiembra.setObservacion(observacion: observacion!)
            tarjetas.append(tarjetaSiembra)
        }
    }
        
        

    
    func getTarjetas() ->[TarjetaSiembraDTO]{
        return tarjetas
    }
    
}
