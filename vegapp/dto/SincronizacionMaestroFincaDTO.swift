//
//  SincronizacionInicialDTO.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class SincronizacionMaestroFincaDTO : JSONUtil {
    
    private var fincas:[FincaDTO] = []
    private var invernaderos:[InvernaderoDTO] = []
    
   


    
    init(json:[String:Any])  {
        super.init()
        setFincasResponse(json:json)
        setInvernaderos(json:json)
    }
   
    public func setFincas(fincas:[FincaDTO]){
        self.fincas = fincas
    }
    
    public func getFincas()-> [FincaDTO]{
        return fincas
    }
    
    func getInvernaderos() -> [InvernaderoDTO]{
        return invernaderos
    }
    
    private func setFincasResponse(json:[String:Any]){
        if let fincaResponse = json["fincas"] as? [[String:Any]]{
            for finca in fincaResponse{
                let codFinca = getStringJsonValue(jsonObject: finca, forKey: "codFinca")
                let nomFinca = getStringJsonValue(jsonObject: finca, forKey: "nomFinca")
                fincas.append(FincaDTO(codFinca: codFinca!,nomFinca: nomFinca!))
            }
        }
    }
    
    private func setInvernaderos(json:[String:Any]){
        var invernaderoDTO:InvernaderoDTO?
        if let invernaderosResponse = json["invernaderos"] as? [[String:Any]]{
            for ivernadero in invernaderosResponse{
                invernaderoDTO = InvernaderoDTO()
                let finca = getJsonObject(jsonObject: ivernadero, forKey: "fincaDTO")
                let codFinca = getStringJsonValue(jsonObject: finca!, forKey: "codFinca")
                invernaderoDTO?.getFincaDTO().setCodFinca(codFinca: codFinca!)
                let modulo = getJsonObject(jsonArray: ivernadero,forKey:"moduloDTO")
                let codModulo = getIntJsonValue(jsonObject:modulo!,forKey:"codModulo")
                invernaderoDTO?.getModuloDTO().setCodModulo(codModulo: codModulo!)
                let nomBloque = getStringJsonValue(jsonObject:ivernadero,forKey:"nomBloque")
                invernaderoDTO?.setNomBloque(nomBloque: nomBloque!)
                let codBloque = getStringJsonValue(jsonObject: ivernadero, forKey: "codBloque")
                invernaderoDTO?.setCodBloque(codBloque: codBloque!)
                invernaderos.append(invernaderoDTO!)
                fincas.append(FincaDTO(codFinca: codFinca!,nomFinca: codFinca!))
            }
        }
    }
    
    
}
