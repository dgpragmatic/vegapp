//
//  InvernaderoDTO.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

 class InvernaderoDTO {
    
    private var fincaDTO:FincaDTO?;
    private var codBloque:String;
    private var nomBloque:String;
    private var moduloDTO:ModuloDTO?;
    
    
    init(){
        codBloque = ""
        nomBloque = ""
    }
    
    public func getFincaDTO() -> FincaDTO{
        if(fincaDTO == nil){
            fincaDTO = FincaDTO()
        }
        return fincaDTO!
    }
    
    public func getCodBloque() -> String {
        return codBloque
    }
    
    public func getNomBloque() ->String{
        return nomBloque
    }
    
    public func getModuloDTO() -> ModuloDTO{
        if(moduloDTO == nil){
            moduloDTO = ModuloDTO()
        }
        return moduloDTO!
    }
    
    public func setCodBloque(codBloque:String){
        self.codBloque = codBloque
    }
    
    public func setNomBloque(nomBloque:String){
        self.nomBloque = nomBloque
    }
    
    
}
