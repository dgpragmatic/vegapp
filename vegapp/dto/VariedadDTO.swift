//
//  VariedadDTO.swift
//  vegapp
//
//  Created by diego garcia on 11/16/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class VariedadDTO{
    
    private var codVar:String
    private var nomVar:String
    private var productoDTO:ProductoDTO?;
    
    init(){
        codVar = ""
        nomVar = ""
    }
    
    func getCodVar() -> String{
        return codVar
    }
    
    func getNomVar() -> String{
        return nomVar
    }
    
    func getProductoDTO() -> ProductoDTO{
        if(productoDTO == nil){
            productoDTO = ProductoDTO()
        }
        return productoDTO!
    }
    
    func setCodVar(codVar:String){
        self.codVar = codVar
    }
    
    func setNomVar(nomVar:String){
        self.nomVar = nomVar
    }
    
    func setProducto(productoDTO:ProductoDTO){
        self.productoDTO = productoDTO
    }
    
    
}
