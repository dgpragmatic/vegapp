//
//  MercadoDTO.swift
//  vegapp
//
//  Created by diego garcia on 11/16/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class MercadoDTO{
    
    private var codMercado:Int?
    private var nomMercado:String?
    
    init(){
        
    }
    
    
    func getCodMercado()->Int{
        return codMercado!
    }
    
    func getNomMercado()->String{
        return nomMercado!
    }
    
    func setCodMercado(codMercado:Int){
        self.codMercado = codMercado
    }
    
    func setNomMercado(nomMercado:String){
        self.nomMercado = nomMercado
    }
    
    
}
