//
//  ActividadTarjetaVO.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class ActividadTarjetaVO {
    
    
    private var nombreActividad:String
    private var fechaProyectada:String
    private var dias:String
    
    
    init(){
        nombreActividad = ""
        fechaProyectada = ""
        dias = ""
    }
    
   
    
    public func getNombreActividad() -> String{
        return nombreActividad
    }
    
    public func getFechaProyectada() -> String{
        return fechaProyectada
    }
    
    public func getDias() ->String{
        return dias
    }
    
    public func setDias(dias:String){
        self.dias = dias
    }
    
    public func setfechaProyectada(fechaProyectada:String){
        self.fechaProyectada = fechaProyectada
    }
    
    public func setNombreActividad(nombreActividad:String){
        self.nombreActividad = nombreActividad
    }
    
   
    
}
    
    

    
  
    
    
   
    
    


