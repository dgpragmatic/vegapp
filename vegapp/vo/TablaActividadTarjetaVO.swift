//
//  TablaActividadTarjetaVO.swift
//  vegapp
//
//  Created by diego garcia on 11/23/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class TablaActividadTarjetaVO{
    
    private var actividad:String
    private var dias:String
    private var fecha:String
    
    init(){
        actividad = ""
        dias = ""
        fecha = ""
    }
    
    func getActividad()->String{
      return actividad
    }
    
    func getDias()->String{
        return dias
    }
    
    func getFecha()->String{
        return fecha
    }
    
    func setActividad(actividad:String){
        self.actividad = actividad
    }
    
    func setDias(dias:String){
        self.dias = dias
    }
    
    func setFecha(fecha:String){
        self.fecha = fecha
    }
    
}
