//
//  DetalleTarjetaVO.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class DetalleTarjetaVO {
    
    private var codBloque:String
    private var nomBloque:String
    private var nroCama:String
    private var cama:Int
    private var codVar:String
    private var nomVar:String
    private var fechaSiem:String
    private var nomPro:String
    private var nomMercado:String
    private var fechaPicoCorte:String
    private var diaPicoCorte:Int
    private var globalState:GlobalState?
    
    init(){
        codBloque = ""
        nomBloque = ""
        nroCama = ""
        cama = 0
        codVar = ""
        nomVar = ""
        fechaSiem = ""
        nomPro = ""
        nomMercado = ""
        fechaPicoCorte = ""
        diaPicoCorte = 0
    }
    
    
    func getDiaPicoCorte() ->Int{
        return diaPicoCorte
    }
    
    func getFechaPicoCorte() ->String{
        return fechaPicoCorte
    }
    
    
    func getNomMercado() ->String{
        return nomMercado
    }
    
    func getNomPro() ->String{
        return nomPro
    }
    
    func getFechaSiem() ->String{
        return fechaSiem
    }
    
    func getNomVar() ->String{
        return nomVar
    }
    
    func getCodVar() ->String{
        return codVar
    }
    
    func getCama() ->Int{
        return cama
    }
    
    func getNroCama() ->String{
        return nroCama
    }
    
    func getNomBloque() ->String{
        return nomBloque
    }
    
    func getCodBloque() ->String{
        return codBloque
    }
    
    
    func setDiaPicoCorte(diaPicoCorte:Int){
        self.diaPicoCorte = diaPicoCorte
    }
    
    func setFechaPicoCorte(fechaPicoCorte:String){
        self.fechaPicoCorte = fechaPicoCorte
    }
    
    func setNomMercado(nomMercado:String){
       self.nomMercado = nomMercado
    }
    
    func setNomPro(nomPro:String){
        self.nomPro = nomPro
    }
    
    func setFechaSiem(fechaSiem:String){
        self.fechaSiem = fechaSiem
    }
    
    func setNomVar(nomVar:String){
        self.nomVar = nomVar
    }
    
    func setCodVar(codVar:String){
        self.codVar = codVar
    }
    
    func setCama(cama:Int){
        self.cama = cama
    }
    
    func setNroCama(nroCama:String){
        self.nroCama = nroCama
    }
    
    func setNomBloque(nomBloque:String){
        self.nomBloque = nomBloque
    }
    
    func setCodBloque(codBloque:String){
        self.codBloque = codBloque
    }
    
    func getGlobalState()->GlobalState{
        return globalState!
    }
    
    func setGlobalState(globalState:GlobalState){
        self.globalState = globalState
    }
    
    
  
    
    
    
}
