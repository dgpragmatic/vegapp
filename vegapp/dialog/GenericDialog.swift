//
//  GenericDialog.swift
//  vegapp
//
//  Created by diego garcia on 11/20/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation
import UIKit

class GenericDialog {
    
    static func showWarning(controller:UIViewController,message:String){
        let alertController = UIAlertController(title: "Advertencia", message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Cerrar", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    
    static func showSincronizeError(controller:UIViewController,param:String){
        
        let message = "No existen " + param + " registradas, favor acercarse a un punto de red y sincronizar el sistema"
        
        let alertController = UIAlertController(title: "Advertencia", message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Cerrar", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    
    static func checkNetworkConnection(controller:UIViewController){
        let isConnected = NetworkUtil.isConnectedToNetwork()
        if(!isConnected){
            showWarning(controller: controller, message: "Es necesario activar el wifi para sicronizar la informacion")
        }
    }
    
    static func getLoading(controller:UIViewController)-> UIActivityIndicatorView {
        let activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator.center = controller.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        activityIndicator.transform = CGAffineTransform(scaleX: 10, y: 10);
        controller.view.addSubview(activityIndicator)
        return activityIndicator
    }
    
    
    
}
