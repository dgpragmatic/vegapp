//
//  SincronizacionMaestroFincaDTO.swift
//  vegapp
//
//  Created by diego garcia on 2/13/19.
//  Copyright © 2019 diego garcia. All rights reserved.
//

import Foundation

class SincronizacionMaestroFincaClient{
    
    var globalState:GlobalState
    
    init(_ globalState:GlobalState){
        self.globalState = globalState
    }
    
    
    public func startSincronizacion() {
        let url = URL(string: EndPoints.sincronizacionMaestrosFinca(finca:(self.globalState.getFincaDTO().getCodFinca()),modulo:(self.globalState.getModulo()),usuario:String("-")))
        var sincronizacionInvernaderoDTO:SincronizacionMaestroFincaDTO?
        let task = URLSession.shared.dataTask(with: url!){(datos:Data?,respuesta:URLResponse?,error:Error?) in
            
            if let datos = datos {
                do{
                    if let jsonResponse = try JSONSerialization.jsonObject(with: datos, options:JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any]{
                        sincronizacionInvernaderoDTO = SincronizacionMaestroFincaDTO(json: jsonResponse)
                        let invernaderoFacade = InvernaderoFacade()
                        invernaderoFacade.insertar(invernaderos: sincronizacionInvernaderoDTO!.getInvernaderos())
                        
                        DispatchQueue.main.async{
                            let invernaderoController = self.globalState.getController() as? SincronizarInvernaderosController
                            invernaderoController?.loadInvernaderos()
                        }
                        
                    }
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
        
    }
    
    
}
