//
//  SincronizacionInicialClient.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class SincronizacionInicialClient{
    
    var globalState:GlobalState
    
    init(_ globalState:GlobalState){
        self.globalState = globalState
    }
   
    
    public func startSincronizacion() {
        let url = URL(string: EndPoints.SINCRONIZACION_INICIAL)
        var sincronizacionInicialDTO:SincronizacionMaestroFincaDTO?
        let fincaFacade = FincaFacade()
        let task = URLSession.shared.dataTask(with: url!){(datos:Data?,respuesta:URLResponse?,error:Error?) in
            
            if let datos = datos {
                do{
                    if let jsonResponse = try JSONSerialization.jsonObject(with: datos, options:JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any]{
                            sincronizacionInicialDTO = SincronizacionMaestroFincaDTO(json: jsonResponse)
                        fincaFacade.insert(fincas:     (sincronizacionInicialDTO?.getFincas())!)
                        
                        DispatchQueue.main.async{
                            let viewController = self.globalState.getController() as? ViewController
                            viewController?.loadFincas()
                        }
                        
                    }
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
        
    }
}
