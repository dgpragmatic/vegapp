//
//  SincronizacionTarjetaClient.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class SincronizacionTarjetaClient {
    
    var globalState:GlobalState?
   
    init(_ globalState:GlobalState){
        self.globalState = globalState
    }
    
    func startSincronizacion(invernaderoDTO:InvernaderoDTO) {
        var sincronizacionTarjetaDTO:SincronizacionTarjetaDTO?
          guard let url = URL(string: EndPoints.sincronizarTarjetas(finca:(self.globalState?.getFincaDTO().getCodFinca())!, bloque: invernaderoDTO.getCodBloque(),modulo:(self.globalState?.getModulo())!)) else {return}
        
        let task = URLSession.shared.dataTask(with: url){(datos:Data?,respuesta:URLResponse?,error:Error?) in
            
            if let datos = datos {
                do{
                    if let jsonResponse = try JSONSerialization.jsonObject(with: datos, options:JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any]{
                        sincronizacionTarjetaDTO = SincronizacionTarjetaDTO(jsonObject:jsonResponse)
                        let tarjetaSiembraFacade = TarjetaSiembraFacade()
                        tarjetaSiembraFacade.insert(tarjetas: (sincronizacionTarjetaDTO?.getTarjetas())!)
                    }
                    DispatchQueue.main.async{
                        let invernaderoController = self.globalState?.getController() as? SincronizarInvernaderosController
                        invernaderoController?.loadInvernaderos()
                    }
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
        
        
        
        
    
    
}
