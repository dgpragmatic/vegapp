//
//  PopupDetalleTarjetaController.swift
//  vegapp
//
//  Created by diego garcia on 11/23/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import UIKit

class PopupDetalleTarjetaController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
   
    
    
    @IBOutlet weak var observacionLb: UILabel!
    
    @IBOutlet weak var picoCorteLb: UILabel!
    @IBOutlet weak var bloqueLb: UILabel!
    @IBOutlet weak var nroCamaLb: UILabel!
    @IBOutlet weak var mercadoLb: UILabel!
    @IBOutlet weak var productoVariedadLb: UILabel!
    private var detalleTarjetaVO:DetalleTarjetaVO?
    private var tarjetaSiembraFacade:TarjetaSiembraFacade?
    private var tarjetas = [TarjetaSiembraDTO]()
    private var fincaSeleccionada:FincaDTO?
    private var codModulo:Int?
    private var listaTablaActividadTarjetaVO = [TablaActividadTarjetaVO]()
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initComponents()
        populateTable()
        
    }
    
    func  initComponents(){
        fincaSeleccionada = detalleTarjetaVO?.getGlobalState().getFincaDTO()
        codModulo = detalleTarjetaVO?.getGlobalState().getModulo()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.nroCamaLb.text = detalleTarjetaVO?.getNroCama()
        self.bloqueLb.text = detalleTarjetaVO?.getNomBloque()
        self.mercadoLb.text = detalleTarjetaVO?.getNomMercado()
        self.productoVariedadLb.text = (detalleTarjetaVO?.getNomPro())! + "-" + (detalleTarjetaVO?.getNomVar())!
        
        let picoCorte = " Día pico corte: " + String(detalleTarjetaVO?.getDiaPicoCorte() ?? 0) + " -- " + (DateUtil.getNameDateByDate(stringDate: (detalleTarjetaVO?.getFechaPicoCorte())!))
        picoCorteLb.text = picoCorte
    }
    
    func populateTable(){
        listaTablaActividadTarjetaVO.removeAll()
        var tablaActividadTarjeta = TablaActividadTarjetaVO()
        tablaActividadTarjeta.setActividad(actividad: "Actividad")
        tablaActividadTarjeta.setDias(dias: "Días")
        tablaActividadTarjeta.setFecha(fecha: "Fecha proyectada")
        listaTablaActividadTarjetaVO.append(tablaActividadTarjeta)
        tarjetaSiembraFacade = TarjetaSiembraFacade()
        tarjetas =  tarjetaSiembraFacade!.findCamaByCodFincaAndCodBloqueAndNroCamaAndModulo(codFinca: fincaSeleccionada!.getCodFinca(), codBloque: detalleTarjetaVO!.getCodBloque(), nroCama: detalleTarjetaVO!.getNroCama(), codModulo: codModulo!)
        if tarjetas.count != 0{
             observacionLb.text = tarjetas[0].getObservacion()
        }else{
           observacionLb.text = ""
        }
       
       
        for tarjeta in tarjetas {
            tablaActividadTarjeta = TablaActividadTarjetaVO()
            
            tablaActividadTarjeta.setActividad(actividad: String (tarjeta.getActividadDTO().getNomActi()))
            tablaActividadTarjeta.setDias(dias: String(tarjeta.getDiasPrograma()))
            tablaActividadTarjeta.setFecha(fecha: String(DateUtil.getNameDateByDate(stringDate: tarjeta.getFechaPrograma())))
             listaTablaActividadTarjetaVO.append(tablaActividadTarjeta)
        }
        
    }
    
    
    @IBAction func closePopup(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    func setDetalleTarjetaVO(detalleTarjetaVO:DetalleTarjetaVO){
        self.detalleTarjetaVO = detalleTarjetaVO
      
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return listaTablaActividadTarjetaVO.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
         let celda = tableView.dequeueReusableCell(withIdentifier: "detalleActividadCell") as! TablaActividadControllerCell
        celda.setTablaActividadTarjetaVO(tablaActividadTarjetaVO: listaTablaActividadTarjetaVO[indexPath.row])
        return celda
    }
    
    
    
    
    

  

}
