//
//  TablaActividadControllerCell.swift
//  vegapp
//
//  Created by diego garcia on 11/23/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import UIKit

class TablaActividadControllerCell: UITableViewCell {

    @IBOutlet weak var lblActividad: UILabel!
    @IBOutlet weak var lblDia: UILabel!
    @IBOutlet weak var lbFecha: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setTablaActividadTarjetaVO(tablaActividadTarjetaVO:TablaActividadTarjetaVO){
        lblActividad.text = tablaActividadTarjetaVO.getActividad()
        lblDia.text = String(tablaActividadTarjetaVO.getDias())
        lbFecha.text = tablaActividadTarjetaVO.getFecha()
    }
    
    
    

}
