//
//  TarjetaController.swift
//  vegapp
//
//  Created by diego garcia on 10/30/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import UIKit

class TarjetaController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
  
    @IBOutlet weak var searchTxt: UITextField!

    private var globalState:GlobalState?
    @IBOutlet weak var btnSincronizarInvernaderos: UIButton!
    @IBOutlet weak var btnSeleccionInvernadero: UIButton!
    
    private var seleccionInvernadero:PopupSeleccionInvernaderoController?
    private var camas = [CamaDTO]()
    private var fincaSeleccionada:FincaDTO?
    private var modulo:Int?
    private var camaSeleccionada:CamaDTO?
    private var detalleTarjetaVO:DetalleTarjetaVO?
    private var invernaderos = [InvernaderoDTO]()
    private var camasTemp  = [CamaDTO]()
    
    @IBOutlet weak var tableCama: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initComponents()
        loadInvernaderos()
    }
    
    private func initComponents(){
        self.title = "Tarjeta "+(globalState?.getFincaDTO().getNomFinca())!
        
        detalleTarjetaVO = DetalleTarjetaVO()
        fincaSeleccionada = globalState?.getFincaDTO()
        modulo = globalState?.getModulo()
        hiddenComponents(isHidden: true)
        searchTxt.addTarget(self, action: #selector(searchRecords(_:)), for: .editingChanged)
    }
    
   
    
    
    
    
    func hiddenComponents(isHidden:Bool){
         self.tableCama.isHidden = isHidden
        self.searchTxt.isHidden = isHidden
    }
    
    @objc func searchRecords(_ textField:UITextField){
        camas.removeAll()
        if textField.text?.count != 0{
            for cama in camasTemp {
                if textField.text != nil {
                      let rangeNroCama = cama.getNroCama().lowercased().range(of: textField.text!,options: .caseInsensitive,range: nil,locale: nil)
                    if rangeNroCama != nil{
                        self.camas.append(cama)
                    }else{
                         let rangeNomVar = cama.getVariedadDTO().getNomVar().lowercased().range(of: textField.text!,options: .caseInsensitive,range: nil,locale: nil)
                        if(rangeNomVar != nil){
                             self.camas.append(cama)
                        }
                        
                    }
                }
            }
        }else{
            for cama in camasTemp {
                camas.append(cama)
            }
        }
        tableCama.reloadData()
    }
    
    private func loadInvernaderos(){
       let invernaderoFacade = InvernaderoFacade()
        invernaderos = invernaderoFacade.findInvernaderoByFincaAndModulo(codFinca:fincaSeleccionada!.getCodFinca(),codModulo:modulo!)
        
        if invernaderos.count == 0 {
              GenericDialog.showWarning(controller: self, message: "Debe sincronizar los invernaderos inicialmente")
        }
    }
    
  
    
    public func setGlobalState(globalState:GlobalState){
        self.globalState = globalState
    }
    
    public func getGlobalState() -> GlobalState{
        return globalState!
    }


    @IBAction func goSincronizarInvernaderos(_ sender: UIButton) {
         self.performSegue(withIdentifier: "sincronizarInvernaderoSegue", sender: "")
    }
    
    
    
    @IBAction func goSeleccionarInvernadero(_ sender: UIButton) {
        self.performSegue(withIdentifier: "seleccionInvernaderoSegue", sender: "")
    }
    
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueIdentifier = segue.identifier
        if segueIdentifier == "sincronizarInvernaderoSegue" {
            let sincronizarInvernaderoController:SincronizarInvernaderosController = segue.destination as! SincronizarInvernaderosController
            sincronizarInvernaderoController.setGlobalState(globalState: globalState!)
        }else if segueIdentifier ==  "seleccionInvernaderoSegue"{
           
                seleccionInvernadero = (segue.destination as! PopupSeleccionInvernaderoController)
                seleccionInvernadero?.setGlobalState(globalState: globalState!)
            
        }else if(segueIdentifier ==  "detalleTarjetaSegue"){
            let popupDetalleTarjetaController:PopupDetalleTarjetaController = segue.destination as! PopupDetalleTarjetaController
            popupDetalleTarjetaController.setDetalleTarjetaVO(detalleTarjetaVO: detalleTarjetaVO!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if seleccionInvernadero != nil{
            btnSeleccionInvernadero.setTitle(seleccionInvernadero?.getGlobalState().getInvernaderoSeleccionado().getNomBloque(), for:.normal)
          seleccionInvernadero = nil
          let tarjetaSiembra = TarjetaSiembraFacade()
            camas = tarjetaSiembra.findCamasByFincaAndBloqueAndModulo(codFinca: (self.fincaSeleccionada?.getCodFinca())!, codBloque: (self.globalState?.getInvernaderoSeleccionado().getCodBloque())!, codModulo: modulo!)
            if camas.count != 0{
                hiddenComponents(isHidden: false)
                tableCama.reloadData()
            }else{
                GenericDialog.showWarning(controller: self, message: "No existen camas relacionadas al bloque seleccionado ")
                hiddenComponents(isHidden: true)
            }
            camasTemp.removeAll()
            for cama in camas{
                camasTemp.append(cama)
            }
        }else{
            btnSeleccionInvernadero.setTitle("Seleccionar un bloque", for:.normal)
        }
    
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return camas.count
    }
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = UITableViewCell()
            celda.textLabel?.text = camas[indexPath.row].getNroCama() + " - " + camas[indexPath.row].getVariedadDTO().getNomVar()
        return celda
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         camaSeleccionada = camas[indexPath.row]
        detalleTarjetaVO?.setCodBloque(codBloque: (globalState?.getInvernaderoSeleccionado().getCodBloque())!)
        detalleTarjetaVO?.setNomBloque(nomBloque: (globalState?.getInvernaderoSeleccionado().getNomBloque())!)
        detalleTarjetaVO?.setNroCama(nroCama: camaSeleccionada!.getNroCama())
        detalleTarjetaVO?.setCama(cama: camaSeleccionada!.getCama())
        detalleTarjetaVO?.setCodVar(codVar: camaSeleccionada!.getVariedadDTO().getCodVar())
        detalleTarjetaVO?.setNomVar(nomVar: camaSeleccionada!.getVariedadDTO().getNomVar())
        detalleTarjetaVO?.setFechaSiem(fechaSiem: camaSeleccionada!.getFechaSiem())
        detalleTarjetaVO?.setNomPro(nomPro: camaSeleccionada!.getVariedadDTO().getProductoDTO().getNomPro())
        detalleTarjetaVO?.setNomMercado(nomMercado: camaSeleccionada!.getMercadoDTO().getNomMercado())
        detalleTarjetaVO!.setFechaPicoCorte(fechaPicoCorte: camaSeleccionada!.getFechaPicoCorte())
        detalleTarjetaVO?.setDiaPicoCorte(diaPicoCorte: camaSeleccionada!.getDiaPicoCorte())
        detalleTarjetaVO?.setGlobalState(globalState:globalState!)
         self.performSegue(withIdentifier: "detalleTarjetaSegue", sender: "")
    }
    
    
    
    
    
}
