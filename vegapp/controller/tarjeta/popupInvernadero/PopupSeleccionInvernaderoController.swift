//
//  PopupSeleccionInvernaderoController.swift
//  vegapp
//
//  Created by diego garcia on 11/22/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import UIKit

class PopupSeleccionInvernaderoController: UIViewController, UITableViewDelegate,UITableViewDataSource {
  
    private var invernaderos = [InvernaderoDTO]()
    private var globalState:GlobalState?
    private var invernaderoSeleccionado:InvernaderoDTO?
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        initComponents()
        loadInvernaderos()
    }
    
    private func initComponents(){
         self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    }
    
    private func loadInvernaderos(){
        let finca = self.globalState?.getFincaDTO()
        let modulo = self.globalState?.getModulo()
        let invernaderoFacade = InvernaderoFacade()
        invernaderoSeleccionado = InvernaderoDTO()
        if self.globalState != nil{
              invernaderos = invernaderoFacade.findInvernaderoByFincaAndModulo(codFinca: (finca?.getCodFinca())!, codModulo: modulo!)
        }
    }
    

    @IBAction func showPopup(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invernaderos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = UITableViewCell()
        celda.textLabel?.text = invernaderos[indexPath.row].getNomBloque()
        return celda
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        globalState?.setInvernaderoSeleccionado(invernaderoSeleccionado: invernaderos[indexPath.row])
        dismiss(animated: true)
    }
    
    
    public func setGlobalState(globalState:GlobalState){
        self.globalState = globalState
    }
    
    public func getGlobalState() -> GlobalState{
        return globalState!
    }

}


