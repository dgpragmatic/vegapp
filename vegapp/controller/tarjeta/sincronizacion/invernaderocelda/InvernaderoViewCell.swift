//
//  InvernaderoViewCell.swift
//  vegapp
//
//  Created by diego garcia on 11/19/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import UIKit

class InvernaderoViewCell: UITableViewCell {
    
    
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var lblInvernadero: UILabel!
    
    @IBOutlet weak var fechaSincrinzacion: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setInvernadero(invernadero:InvernaderoTarjetaDTO,fechaSincronizacion:String){
       self.lblInvernadero.text = invernadero.getInvernaderoDTO().getNomBloque()
       self.checkBtn.isSelected = invernadero.isSelected()!
        if fechaSincronizacion != ""{
              self.fechaSincrinzacion.text = fechaSincronizacion
        }else{
            self.fechaSincrinzacion.text = "Sin Sincronizar"
        }
        
        
     
    }
    
    func getCheckBtn()-> UIButton{
        return checkBtn
    }

}
