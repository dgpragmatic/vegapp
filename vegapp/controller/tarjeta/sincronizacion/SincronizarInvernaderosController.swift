//
//  SincronizarInvernaderosController.swift
//  vegapp
//
//  Created by diego garcia on 10/30/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import UIKit

class SincronizarInvernaderosController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
   
    @IBOutlet weak var tableInvernadero: UITableView!
    private var globalState:GlobalState?
    var invernaderoFacade:InvernaderoFacade?
    var invernaderos = [InvernaderoDTO]()
    var invernaderosTarjeta = [InvernaderoTarjetaDTO]()
    var fincaSeleccionada:FincaDTO?
    var moduloSeleccionado:Int?
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var sincronizacionFacade:SincronizacionFacade?
    var tarjetaSiembraFacade:TarjetaSiembraFacade?
    var isFirstLoading:Bool?
    var contadorInvernaderos = 0
  
    @IBOutlet weak var btnSincronizarTarjeta: UIButton!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        GenericDialog.checkNetworkConnection(controller: self)
        initComponents()
        sincronizarInvernaderos()
        activityIndicator = GenericDialog.getLoading(controller: self)
        loadInvernaderos()
       
    }
    
    
    
    
    private func initComponents(){
         self.title = "Sincronización invernaderos "+(globalState?.getFincaDTO().getNomFinca())!

        isFirstLoading = true
        tableInvernadero.dataSource = self
        tableInvernadero.delegate = self
        fincaSeleccionada = globalState!.getFincaDTO()
        moduloSeleccionado =  globalState!.getModulo()
        globalState?.setController(controller: self)
        sincronizacionFacade = SincronizacionFacade(globalState: globalState!)
        tarjetaSiembraFacade = TarjetaSiembraFacade()
    }
    
    func loadInvernaderos(){
        invernaderosTarjeta.removeAll()
        tableInvernadero.reloadData()
        activityIndicator.startAnimating()
        invernaderoFacade = InvernaderoFacade()
        invernaderos = invernaderoFacade!.findInvernaderoByFincaAndModulo(codFinca:fincaSeleccionada!.getCodFinca(),codModulo:moduloSeleccionado!)
        for invernadero in invernaderos{
            let invernaderoTarjeta = InvernaderoTarjetaDTO()
            invernaderoTarjeta.setInvernaderoDTO(invernaderoDTO: invernadero)
           
            invernaderosTarjeta.append(invernaderoTarjeta)
        }
      
        if(isFirstLoading!){
            activityIndicator.stopAnimating()
        }else{
            contadorInvernaderos -= 1
            if contadorInvernaderos == 0{
                activityIndicator.stopAnimating()
                // se desbloquea pantalla al cerrar loading
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
        tableInvernadero.reloadData()
        
    }
    
    
    private func sincronizarInvernaderos(){
        sincronizacionFacade?.sincronizarMaestrosFincas()
    }
    
    public func setGlobalState(globalState:GlobalState){
        self.globalState = globalState
    }
    
    public func getGlobalState() -> GlobalState{
        return globalState!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invernaderos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let codFinca = fincaSeleccionada?.getCodFinca()
        let codBloque = invernaderosTarjeta[indexPath.row].getInvernaderoDTO().getCodBloque()
        
        let celda = tableView.dequeueReusableCell(withIdentifier: "invernaderoCell") as! InvernaderoViewCell
        let fechaSincronizacion = tarjetaSiembraFacade?.findFechaSincronizacionByFincaAndBloqueAndModulo(codFinca: codFinca!, codBloque: codBloque, codModulo: moduloSeleccionado!)
        celda.setInvernadero(invernadero: invernaderosTarjeta[indexPath.row], fechaSincronizacion:fechaSincronizacion! )
        getBackGround(indexPath: indexPath, fechaSincronizacion: fechaSincronizacion!)
        celda.backgroundColor   = invernaderosTarjeta[indexPath.row].getBackGroundColor()
        celda.selectionStyle = .none
        celda.getCheckBtn().addTarget(self, action: #selector(checkButtonClicked(sender:)), for: .touchUpInside)
        return celda
       
    }
    
    private func getBackGround(indexPath: IndexPath,fechaSincronizacion:String){
        
         var diferencia:Int = -1
    
        if(fechaSincronizacion != ""){
            let fechaSincronizacionDate = DateUtil.convertStringToDateNotHour(stringDate: fechaSincronizacion)
            let fechaActualDate = DateUtil.convertStringToDateNotHour(stringDate: DateUtil.getCurrentDate())
            diferencia =  DateUtil.daysBettweenDates(startDate: fechaSincronizacionDate, endDate: fechaActualDate)
        }
        
        
        var colorBackGround = ""
        if(fechaSincronizacion == ""){
            colorBackGround = ColorsConstants.ROSADO
        }else if diferencia > 0 {
            colorBackGround = ColorsConstants.AMARILLO;
        }else{
            colorBackGround = ColorsConstants.VERDE
        }
       
        
        
        let backGround = UIColor(hexString: colorBackGround)
        invernaderosTarjeta[indexPath.row].setBackGroundColor(backGround: backGround)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    
    
    
    @objc func checkButtonClicked(sender:UIButton){
        let cell = sender.superview?.superview as! InvernaderoViewCell
        let indexPath = tableInvernadero.indexPath(for: cell)
        if sender.isSelected{
            sender.isSelected = false
            self.invernaderosTarjeta[indexPath!.row].setSelected(selected:false)
            
        }else{
             sender.isSelected = true
            self.invernaderosTarjeta[indexPath!.row].setSelected(selected:true)
        }
        tableInvernadero.reloadData()
    }
    
    
    

    
    func getTableInvernaderos()-> UITableView!{
        return tableInvernadero
    }

    
    
    @IBAction func sincrinizarTarjeta(_ sender: UIButton) {
      
        isFirstLoading! = false
        GenericDialog.checkNetworkConnection(controller: self)
        if(NetworkUtil.isConnectedToNetwork()){
            contadorInvernaderos = 0
            var invernaderosSeleccionados = [InvernaderoDTO]()
            for invernadero in invernaderosTarjeta {
                if invernadero.isSelected() {
                    invernaderosSeleccionados.append(invernadero.getInvernaderoDTO())
                    contadorInvernaderos += 1
                }
            }
            
            if contadorInvernaderos == 0{
                GenericDialog.showWarning(controller: self, message: "Debe seleccionar al menos un invernadero")
            }else{
                // se bloquea pantalla
                UIApplication.shared.beginIgnoringInteractionEvents()
                activityIndicator.startAnimating()
                sincronizacionFacade?.sincronizarTarjeta(invernaderos: invernaderosSeleccionados)
            }
        }
    }
    
    
}
