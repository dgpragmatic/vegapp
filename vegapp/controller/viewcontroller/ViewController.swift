//
//  ViewController.swift
//  vegapp
//
//  Created by diego garcia on 10/30/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var globalState:GlobalState?
    var popupSeleccionFinca:PopupSeleccionFincaController?
    
    @IBOutlet weak var btnSincrinzacion: UIButton!
    
    @IBOutlet weak var btnSeleccionFinca: UIButton!
    @IBOutlet weak var btnModulo: UIButton!
    
    @IBOutlet weak var tablaModulo: UITableView!
      var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var fincas = [FincaDTO]()
    
    
    var modulos = [ModuloDTO]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GenericDialog.checkNetworkConnection(controller: self)
        activityIndicator = GenericDialog.getLoading(controller: self)
        initComponents()
        sincronizacionInicial()
        loadFincas()
        loadModulos()
        
    }
    
    func initComponents(){
        globalState = GlobalState()
        self.tablaModulo.isHidden = true
    }
    
    func sincronizacionInicial(){
        globalState?.setController(controller: self)
        let sincronizacionFacade = SincronizacionFacade(globalState: globalState!)
        sincronizacionFacade.getSincronizacionIncial()
    }
    
    func loadFincas(){
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        let fincaFacade = FincaFacade()
        fincas = fincaFacade.listAll()
        activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    

    
    
    func loadModulos() {
        modulos.append(ModuloDTO(codModulo: Modulo.TIPO_MODULO_PRODUCCION,nomModulo: "Producción"))
        modulos.append(ModuloDTO(codModulo: Modulo.TIPO_MODULO_PROPAGACION,nomModulo: "Propagacion"))
    }
    
    @IBAction func goPopupSeleccionFinca(_ sender: UIButton) {
        self.performSegue(withIdentifier: "popupSeleccionSegue", sender: "")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let segueIdentifier = segue.identifier
        
        if segueIdentifier == "popupSeleccionSegue" {
            if(fincas.count != 0){
                let popupSeleccionFinca:PopupSeleccionFincaController = segue.destination as!
                PopupSeleccionFincaController
                popupSeleccionFinca.setGlobalState(globalState: globalState!)
                self.popupSeleccionFinca = popupSeleccionFinca
            }else{
                GenericDialog.showSincronizeError(controller: self, param: "fincas")
            }
        }else if segueIdentifier == "tarjetaViewSegue"{
            
            if globalState?.getFincaDTO().getCodFinca() == ""{
                GenericDialog.showWarning(controller: self, message: "Favor seleccionar una finca")
            }else if globalState?.getModulo() == 0{
                GenericDialog.showWarning(controller: self, message: "Favor seleccionar un módulo")
            } else{
                let tarjetaController:TarjetaController = segue.destination as! TarjetaController
                tarjetaController.setGlobalState(globalState: globalState!)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if globalState?.getFincaDTO().getNomFinca() == "" {
            btnSeleccionFinca.setTitle("Seleccionar finca", for:.normal)
        }else{
            btnSeleccionFinca.setTitle(globalState?.getFincaDTO().getNomFinca(), for:.normal)
        }
    }
    
    
    
    
    func animate(toogle: Bool){
        if toogle {
            UIView.animate(withDuration: 0.3){
                self.tablaModulo.isHidden = false
            }
        }else{
            UIView.animate(withDuration: 0.3){
                self.tablaModulo.isHidden = true
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modulos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = UITableViewCell()
        celda.textLabel?.text = self.modulos[indexPath.row].getNomModulo()
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        btnModulo.setTitle(modulos[indexPath.row].getNomModulo(), for: .normal)
        globalState?.setModulo(modulo: modulos[indexPath.row].getCodModulo())
        animate(toogle: false)
    }
    
   
    @IBAction func onClickDropDown(_ sender: UIButton) {
        if tablaModulo.isHidden{
            animate(toogle: true)
        }else{
            animate(toogle: false)
        }
    }
    
    
    @IBAction func sincronizacionInicialListener(_ sender: UIButton) {
         GenericDialog.checkNetworkConnection(controller: self)
         sincronizacionInicial()
    }
    
    

    
}

