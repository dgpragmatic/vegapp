//
//  PopupSeleccionFincaController.swift
//  vegapp
//
//  Created by diego garcia on 11/9/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//
//https://www.youtube.com/watch?v=ouOqBMtULxI
import UIKit

class PopupSeleccionFincaController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var btnClosePopup: UIButton!
    
    private var globalState:GlobalState?
    private var fincas = [FincaDTO]()
    private var fincaSelected:FincaDTO?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        loadFincas()
    }
    
    func loadFincas(){
        let fincaFacade = FincaFacade()
        fincas = fincaFacade.listAll()
    }
    
    
    
    @IBAction func closePopup(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fincas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let celda = UITableViewCell()
        celda.textLabel?.text = fincas[indexPath.row].getNomFinca()
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        globalState?.setFincaDTO(fincaDTO: fincas[indexPath.row])
        dismiss(animated: true)
    }
    
    
    
    
    public func setGlobalState(globalState:GlobalState){
        self.globalState = globalState
    }
    
    public func getGlobalState() -> GlobalState{
        return globalState!
    }
    
    
    
    @IBAction func close(_ sender: UIButton) {
         dismiss(animated: true)
    }
    
    
    
}
