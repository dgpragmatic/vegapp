//
//  ColorsConstants.swift
//  vegapp
//
//  Created by diego garcia on 11/22/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

struct ColorsConstants{
    
     static let AMARILLO = "#FFFF7E"
     static let AZUL = "#138DCE";
     static let BLANCO = "#FFFFFF";
     static let NEGRO = "#000000";
     static let ROSADO = "#FBB9B9";
     static let VERDE = "#77CF52";
     static let VIOLETA = "#8C43A3";

    
    
}
