//
//  GlobalState.swift
//  vegapp
//
//  Created by diego garcia on 11/10/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation
import UIKit

class GlobalState{
    
    private var fincaDTO:FincaDTO?;
    private var modulo:Int
    private var invernaderoSeleccionado:InvernaderoDTO?
    private var controller:UIViewController?
    
    
    
    init(){
        modulo = 0
    }
    
    public func setFincaDTO(fincaDTO:FincaDTO) {
        self.fincaDTO = fincaDTO
    }
    
    public func  getFincaDTO() -> FincaDTO {
        if(fincaDTO == nil){
            fincaDTO = FincaDTO()
        }
       return fincaDTO!
    }
    
    public func getModulo()->Int{
        return modulo
    }
    
    public func setModulo(modulo:Int){
        self.modulo = modulo
    }
    
    public func setInvernaderoSeleccionado(invernaderoSeleccionado:InvernaderoDTO){
        self.invernaderoSeleccionado = invernaderoSeleccionado
    }
    
    public func getInvernaderoSeleccionado()-> InvernaderoDTO{
        if invernaderoSeleccionado == nil{
            invernaderoSeleccionado = InvernaderoDTO()
        }
       return invernaderoSeleccionado!
    }
    
    public func setController(controller:UIViewController){
        self.controller = controller
    }
    
    public func getController()->UIViewController{
        return controller!
    }
    
    
    
    
    
    
}
