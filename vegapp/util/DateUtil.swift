//
//  DateUtil.swift
//  vegapp
//
//  Created by diego garcia on 11/21/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class DateUtil{
    
    static let DATE_FORMAT_HOUR = "yyyy-MM-dd HH:mm:ss"
    static let DATE_FORMAT_NOT_HOUR = "yyyy-MM-dd"
    
    static func getCurrentDate() -> String{
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATE_FORMAT_HOUR
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    static func daysBettweenDates(startDate:Date,endDate:Date)->Int{
        let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year])
        let differenceOfDate = Calendar.current.dateComponents(components, from: startDate, to: endDate)
        return differenceOfDate.day!
    }
    
    
    static func convertStringToDateNotHour(stringDate:String)->Date{
        let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = DATE_FORMAT_HOUR
        let formatedStartDate = dateFormatter.date(from: stringDate)
        let components = Calendar.current.dateComponents([.year, .month, .day], from: formatedStartDate!)
        let date = Calendar.current.date(from: components)
        return date!
    }
    
    
    static func convertStringToDate(stringDate:String)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATE_FORMAT_HOUR
        let formatedStartDate = dateFormatter.date(from: stringDate)
        return formatedStartDate!
    }
    
    
    
    static func getNameDateByDate(stringDate:String) -> String {
        let formatter  = DateFormatter()
        formatter.dateFormat = DATE_FORMAT_NOT_HOUR
        guard let todayDate = formatter.date(from: stringDate) else { return "" }
        let myCalendar = Calendar(identifier: .gregorian)
        let month = myCalendar.component(.month, from: todayDate)
        let months =  ["ene","feb","mar","abr","may",
            "jun","jul","ago","sep","oct","nov","dic"]
        let day =  myCalendar.component(.day, from: todayDate)
        let year =  myCalendar.component(.year, from: todayDate)
        return String(day)+"-"+months[month-1]+"-"+String(year)
    }
    
    
    
    
    
    
    
    
}
