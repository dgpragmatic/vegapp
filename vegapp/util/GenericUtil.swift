//
//  GenericUtil.swift
//  vegapp
//
//  Created by diego garcia on 11/21/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class GenericUtil{
    
    static func getRandomId() -> String {
        let length = 30
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    
    
    
    
}
