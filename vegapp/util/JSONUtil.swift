//
//  JSONUtil.swift
//  vegapp
//
//  Created by diego garcia on 11/16/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class JSONUtil{
    
    
    init(){}
 
    
    func getJsonArray(jsonArray:[String:Any], forKey:String) -> [[String:Any]]?{
        guard let value = jsonArray[forKey] as? [[String:Any]] else { return nil}
        return value
    }
    
    
    
    func getJsonObject(jsonArray:[String:Any], forKey:String) -> [String:Any]? {
           guard let value = jsonArray[forKey] as? [String: Any] else{return  nil }
         return value
    }
    
    
    func getJsonObject(jsonObject:[String:Any], forKey:String) -> [String:Any]?{
        guard let value = jsonObject[forKey] as? [String:Any] else {return  nil }
        return value
    }
    
    func getStringJsonValue(jsonObject:[String:Any], forKey:String) -> String?{
        guard let value = jsonObject[forKey] as? String else {return  "" }
        return value
    }
    
    func getIntJsonValue(jsonObject:[String:Any], forKey:String) -> Int?{
        guard let value = jsonObject[forKey] as? Int else {return  0 }
        return value
    }
    
  
  
    
   
    
   
    
    
    
}
