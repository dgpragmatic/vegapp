//
//  TarjetaSiembraFacade.swift
//  vegapp
//
//  Created by diego garcia on 11/21/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class TarjetaSiembraFacade{
    
    var tarjetaSiembraBusiness:TarjetaSiembraBusiness
    
    init(){
        tarjetaSiembraBusiness = TarjetaSiembraBusiness()
    }
    
    
    func insert(tarjetas:[TarjetaSiembraDTO]){
        tarjetaSiembraBusiness.insert(tarjetas: tarjetas)
    }
    
    func findCamasByFincaAndBloqueAndModulo(codFinca:String, codBloque:String, codModulo:Int) ->[CamaDTO]{
       return tarjetaSiembraBusiness.findCamasByFincaAndBloqueAndModulo(codFinca: codFinca, codBloque: codBloque, codModulo: codModulo)
    }
    
    func findFechaSincronizacionByFincaAndBloqueAndModulo(codFinca:String, codBloque:String, codModulo:Int)->String{
        return tarjetaSiembraBusiness.findFechaSincronizacionByFincaAndBloqueAndModulo(codFinca: codFinca, codBloque: codBloque,codModulo:codModulo)
    }
    
    func findCamaByCodFincaAndCodBloqueAndNroCamaAndModulo(codFinca:String,codBloque:String,nroCama:String,codModulo:Int)->[TarjetaSiembraDTO]{
        
        let tarjetas = tarjetaSiembraBusiness.findCamaByCodFincaAndCodBloqueAndNroCamaAndModulo(codFinca:codFinca,codBloque:codBloque,nroCama:nroCama,codModulo:codModulo)
        return tarjetas
    }
    
    
    
    
}
