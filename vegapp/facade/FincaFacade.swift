//
//  FincaFacade.swift
//  vegapp
//
//  Created by diego garcia on 11/8/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class FincaFacade{
    
    var fincaBusiness:FincaBusiness
    
    init(){
        fincaBusiness = FincaBusiness()
    }
    
    func insert(fincas:[FincaDTO]){
        fincaBusiness.insert(fincas:fincas)
    }
    
    func listAll() -> [FincaDTO]{
        return fincaBusiness.listAll()
    }
    
    
    
    
}
