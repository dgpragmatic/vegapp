//
//  SincronizacionFacade.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

 class SincronizacionFacade{
    
    private var sincronizacionBusiness: SincronizeBusiness
    
    init(){
        sincronizacionBusiness = SincronizeBusiness()
    }
    
    init(globalState:GlobalState){
        sincronizacionBusiness = SincronizeBusiness(globalState)
    }
    
    public func sincronizarTarjeta(invernaderos:[InvernaderoDTO]) {
        sincronizacionBusiness.sincronizarTarjeta(invernaderos: invernaderos)
    }
    
    public func getSincronizacionIncial() {
        sincronizacionBusiness.getSincronizacionInicial()
    }
    
    public func sincronizarMaestrosFincas(){
        sincronizacionBusiness.sincronizarMestrosFincas()
    }
    
}
