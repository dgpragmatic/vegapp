//
//  InvernaderoFacade.swift
//  vegapp
//
//  Created by diego garcia on 11/14/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class InvernaderoFacade{
    
    var invernaderoBusiness:InvernaderoBusiness
    
    init() {
        invernaderoBusiness = InvernaderoBusiness()
    }
    
    func insertar(invernaderos:[InvernaderoDTO]){
         invernaderoBusiness.insertar(invernaderos: invernaderos)
    }
    
    func findInvernaderoByFincaAndModulo(codFinca:String, codModulo:Int )->[InvernaderoDTO]{
        return invernaderoBusiness.findInvernaderoByFincaAndModulo(codFinca:codFinca, codModulo:codModulo )
    }
    
    
    
    
}
