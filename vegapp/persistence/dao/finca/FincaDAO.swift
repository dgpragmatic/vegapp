//
//  FincaDAO.swift
//  vegapp
//
//  Created by diego garcia on 11/8/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

import SQLite3

class FincaDAO : DAOUtil{
    

    override
    init(db:OpaquePointer){
       super.init(db: db)
    }
    
    func clear(){
        let queryString = "DELETE from finca"
        prepareStarement(queryString)
        done()
    }
    
    func insert(fincaDTO:FincaDTO){
        let queryString = "INSERT INTO finca (codFinca, nomFinca) VALUES (?,?)"
        prepareStarement(queryString)
        setText(text:fincaDTO.getCodFinca(),index:1)
        setText(text:fincaDTO.getNomFinca(),index: 2)
        done()
    }
    
    func listAll() -> [FincaDTO]{
        var fincas = [FincaDTO]()
        let queryString = "SELECT * from finca"
        prepareStarement(queryString)
        while nextRow() {
            let codFinca = getString(columIndex: 0)
            let nomFinca = getString(columIndex: 1)
            fincas.append(FincaDTO(codFinca: codFinca, nomFinca: nomFinca))
        }
        return fincas
    }
    
    
}
