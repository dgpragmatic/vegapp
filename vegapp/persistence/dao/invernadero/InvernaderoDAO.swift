//
//  InvernaderoDAO.swift
//  vegapp
//
//  Created by diego garcia on 11/14/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class InvernaderoDAO:DAOUtil{
    
    override
    init(db:OpaquePointer){
        super.init(db: db)
    }
    
    func clear(invernaderoDTO:InvernaderoDTO){
        let queryString = "DELETE from invernadero WHERE codFinca=? AND codModulo=?"
        prepareStarement(queryString)
        setText(text:invernaderoDTO.getNomBloque(),index: 1)
        setInt(int:invernaderoDTO.getModuloDTO().getCodModulo(),index:2)
        done()
    }
    
    func insert(invernaderoDTO:InvernaderoDTO){
        let queryString = "INSERT INTO invernadero (codBloque,nomBloque, codModulo, codFinca) VALUES (?,?,?,?)"
        prepareStarement(queryString)
        setText(text:invernaderoDTO.getCodBloque(),index:1)
        setText(text:invernaderoDTO.getNomBloque(),index: 2)
        setInt(int:invernaderoDTO.getModuloDTO().getCodModulo(),index:3)
        setText(text:invernaderoDTO.getFincaDTO().getCodFinca(),index:4)
        done()
    }
    
    
    func findInvernaderoByFincaAndModulo(codFinca:String, codModulo:Int )->[InvernaderoDTO]{
        var invernaderos = [InvernaderoDTO]()
        let queryString = "SELECT * from invernadero WHERE codFinca='"+codFinca+"' AND codModulo="+String(codModulo)
        prepareStarement(queryString)
        while nextRow() {
            let invernadero = InvernaderoDTO()
            let codBloque = getString(columIndex: 0)
            invernadero.setCodBloque(codBloque: codBloque)
            let codModulo = Int(getString(columIndex: 1))
            invernadero.getModuloDTO().setCodModulo(codModulo: codModulo!)
            let nomBloque = getString(columIndex: 2)
            invernadero.setNomBloque(nomBloque:nomBloque)
            let codFinca = getString(columIndex: 3)
            invernadero.getFincaDTO().setCodFinca(codFinca: codFinca)
            invernaderos.append(invernadero)
        }
        return invernaderos
    }
    
    
    
}
