//
//  TarjetaSiembraDAO.swift
//  vegapp
//
//  Created by diego garcia on 11/21/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class TarjetaSiembraDAO:DAOUtil{
    
    override
    init(db:OpaquePointer){
        super.init(db: db)
    }
    
    func clear(tarjetaSiembraDTO:TarjetaSiembraDTO){
        let queryString  = "DELETE FROM tarjeta_siembra WHERE codFinca = ? AND codBloque = ? AND codModulo = ? "
         prepareStarement(queryString)
         setText(text:tarjetaSiembraDTO.getCamaDTO().getInvernaderoDTO().getFincaDTO().getCodFinca(),index: 1); setText(text:tarjetaSiembraDTO.getCamaDTO().getInvernaderoDTO().getCodBloque(),index:2); setInt(int:tarjetaSiembraDTO.getCamaDTO().getInvernaderoDTO().getModuloDTO().getCodModulo(),index:3)
        done()
    }
    
    func insert(tarjetaSiembraDTO:TarjetaSiembraDTO){
        let queryString =  "INSERT INTO tarjeta_siembra (id,codFinca,codBloque,nombloque,codModulo,cama,nroCamas,fechaSiem,codMercado,nomMercado,codPro,nomPro,codVar,nomVar,codActi,nomActi,fechaPrograma,diasPrograma,fechaPicoCorte,diaPicoCorte,fechaSincronizacion,observacion) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        prepareStarement(queryString)
       
        tarjetaSiembraDTO.setId(id: GenericUtil.getRandomId())
        setText(text:tarjetaSiembraDTO.getId(), index: 1);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getInvernaderoDTO().getFincaDTO().getCodFinca(), index: 2);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getInvernaderoDTO().getCodBloque(),index: 3);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getInvernaderoDTO().getNomBloque(),index: 4);
        setInt(int:tarjetaSiembraDTO.getCamaDTO().getInvernaderoDTO().getModuloDTO().getCodModulo(), index: 5);
        setInt(int:tarjetaSiembraDTO.getCamaDTO().getCama(), index: 6);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getNroCama(), index: 7);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getFechaSiem(), index: 8);
        setInt(int:tarjetaSiembraDTO.getCamaDTO().getMercadoDTO().getCodMercado(),index: 9);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getMercadoDTO().getNomMercado(), index: 10);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getVariedadDTO().getProductoDTO().getCodPro(),index: 11);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getVariedadDTO().getProductoDTO().getNomPro(),index: 12);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getVariedadDTO().getCodVar(),index: 13);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getVariedadDTO().getNomVar(),index: 14);
        setText(text:tarjetaSiembraDTO.getActividadDTO().getCodActi(),index: 15);
        setText(text:tarjetaSiembraDTO.getActividadDTO().getNomActi(),index: 16);
        setText(text:tarjetaSiembraDTO.getFechaPrograma(),index: 17);
        setInt(int:tarjetaSiembraDTO.getDiasPrograma(),index: 18);
        setText(text:tarjetaSiembraDTO.getCamaDTO().getFechaPicoCorte(),index: 19);
        setInt(int:tarjetaSiembraDTO.getCamaDTO().getDiaPicoCorte(),index: 20);
        tarjetaSiembraDTO.setFechaSincronizacion(fechaSincronizacion: DateUtil.getCurrentDate())
        setText(text:tarjetaSiembraDTO.getFechaSincronizacion(),index: 21);
        setText(text:tarjetaSiembraDTO.getObservacion(), index: 22);
        done()
    }
    
    func findCamasByFincaAndBloqueAndModulo(codFinca:String, codBloque:String, codModulo:Int)-> [CamaDTO]{
       var camas = [CamaDTO]()
        let queryString = "SELECT DISTINCT nroCamas,cama,codVar,nomVar,fechaSiem,nomPro,nomMercado,fechaPicoCorte,diaPicoCorte  FROM tarjeta_siembra WHERE codFinca = '"+codFinca+"' AND codBloque = '"+codBloque+"' AND codModulo = "+String(codModulo)+" ORDER BY cama,nroCamas ASC";
        
        
        prepareStarement(queryString)
        while nextRow() {
            let camaDTO = CamaDTO()
            camaDTO.setNroCama(nroCama: getString(columIndex: 0));
            camaDTO.setCama(cama: getInt(columIndex: 1))
            camaDTO.getVariedadDTO().setCodVar(codVar: getString(columIndex: 2))
            camaDTO.getVariedadDTO().setNomVar(nomVar: getString(columIndex: 3));
            camaDTO.setFechaSiem(fechaSiem: getString(columIndex: 4));
            camaDTO.getVariedadDTO().getProductoDTO().setNomPro(nomPro: getString(columIndex: 5));
            camaDTO.getMercadoDTO().setNomMercado(nomMercado: getString(columIndex: 6));
            camaDTO.setFechaPicoCorte(fechaPicoCorte: getString(columIndex: 7));
            camaDTO.setDiaPicoCorte(diaPicoCorte: getInt(columIndex: 8));
           camas.append(camaDTO)
        }
        return camas
        
    }
    
    
    func findFechaSincronizacionByFincaAndBloqueAndModulo(codFinca:String, codBloque:String, codModulo:Int)-> String{
            var fechaSincronizacion = "";
            let queryString  = "SELECT MAX(fechaSincronizacion) AS fechaSincronizacion FROM tarjeta_siembra WHERE codFinca = '"+codFinca+"' AND codBloque = '"+codBloque+"' AND codModulo = "+String(codModulo);
            prepareStarement(queryString)
            while nextRow() {
                   fechaSincronizacion = getString(columIndex: 0)
            }
            return fechaSincronizacion
    }
    
    func findCamaByCodFincaAndCodBloqueAndNroCamaAndModulo(codFinca:String,codBloque:String,nroCama:String,codModulo:Int)-> [TarjetaSiembraDTO]{
        var tarjetas = [TarjetaSiembraDTO]()
        let queryString = "SELECT nroCamas,cama,codVar,nomVar,fechaSiem,nomPro,nomMercado,fechaPicoCorte,nomActi,fechaPrograma,diasPrograma,observacion  FROM tarjeta_siembra WHERE codFinca = '" + codFinca + "' AND codBloque = '" + codBloque + "' AND nroCamas = '" + nroCama + "' AND codModulo = "+String(codModulo) + " ORDER BY diasPrograma ASC"
        prepareStarement(queryString)
        while nextRow() {
            let tarjetaSiembraDTO =  TarjetaSiembraDTO();
            tarjetaSiembraDTO.getCamaDTO().setNroCama(nroCama: getString(columIndex: 0))
            tarjetaSiembraDTO.getCamaDTO().setCama(cama: getInt(columIndex: 1))
            tarjetaSiembraDTO.getCamaDTO().getVariedadDTO().setCodVar(codVar: getString(columIndex: 2))
            tarjetaSiembraDTO.getCamaDTO().getVariedadDTO().setNomVar(nomVar: getString(columIndex: 3));
            tarjetaSiembraDTO.getCamaDTO().setFechaSiem(fechaSiem: getString(columIndex: 4));
            tarjetaSiembraDTO.getCamaDTO().getVariedadDTO().getProductoDTO().setNomPro(nomPro: getString(columIndex: 5));
            tarjetaSiembraDTO.getCamaDTO().getMercadoDTO().setNomMercado(nomMercado: getString(columIndex: 6));
            tarjetaSiembraDTO.getCamaDTO().setFechaPicoCorte(fechaPicoCorte: getString(columIndex: 7));
            tarjetaSiembraDTO.getActividadDTO().setNomActi(nomActi: getString(columIndex: 8));
            tarjetaSiembraDTO.setFechaPrograma(fechaPrograma: getString(columIndex: 9));
            tarjetaSiembraDTO.setDiasPrograma(diasPrograma: getInt(columIndex: 10));
            tarjetaSiembraDTO.setObservacion(observacion: getString(columIndex: 11));
            tarjetas.append(tarjetaSiembraDTO)
        }
        return tarjetas
        
    }
    
}
