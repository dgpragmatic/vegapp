//
//  TableBuilder.swift
//  vegapp
//
//  Created by diego garcia on 11/8/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

import SQLite3

class TableBuilder{
    
    let db:OpaquePointer
    
    init(db:OpaquePointer){
        self.db = db
    }
    
    
    func createTables(){
        
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS finca (codFinca TEXT PRIMARY KEY, nomFinca TEXT)", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
        
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS invernadero (codBloque TEXT, codModulo TEXT , nomBloque TEXT, codFinca TEXT, FOREIGN KEY(codFinca) REFERENCES finca(id), PRIMARY KEY(codBloque, codFinca, codModulo))", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
        
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS tarjeta_siembra (id TEXT PRIMARY KEY,codFinca TEXT, codBloque TEXT, nombloque TEXT,codModulo INTEGER, cama INTEGER, nroCamas TEXT, fechaSiem TEXT, codMercado TEXT, nomMercado TEXT, codPro TEXT, nomPro TEXT, codVar TEXT, nomVar TEXT, codActi TEXT, nomActi TEXT, fechaPrograma TEXT, diasPrograma INTEGER, fechaPicoCorte TEXT, diaPicoCorte INTEGER, fechaSincronizacion TEXT,observacion TEXT)", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
        
    }
}
