//
//  DAOUtil.swift
//  vegapp
//
//  Created by diego garcia on 11/8/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation
import SQLite3

class DAOUtil{

    
    var db:OpaquePointer?
    var stmt: OpaquePointer?
  
    
    init(db:OpaquePointer){
         self.db = db
    }
    
    func prepareStarement(_ queryString:String){
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return
        }
    }
    
    func done(){
        if sqlite3_step(stmt) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure inserting table: \(errmsg)")
            return
        }
    }
    
    func setText( text:String,  index:Int32){
        let text = text as NSString
        if sqlite3_bind_text(stmt, index, text.utf8String, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding filed String: \(errmsg)")
            return
        }
    }
    
    func setInt(int:Int,  index:Int32){
        let value = String(int) as NSString
        if sqlite3_bind_int(stmt,index, value.intValue) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding filed String: \(errmsg)")
            return
        }
    }
    
    
    func getString( columIndex:Int32 )->String{
        let result = sqlite3_column_text(stmt, columIndex)
        var resultString = ""
        if result != nil{
             resultString = String(cString: result! )
        }
        return resultString
    }
    
 
    
    
    
    
    func getInt( columIndex:Int32 )->Int{
        return Int(String(cString: sqlite3_column_text(stmt, columIndex))) ?? 0
    }
   
   
    
    
    func nextRow()->Bool{
        return sqlite3_step(stmt) == SQLITE_ROW
    }
    
   
   
    
    
    
}
