//
//  QueryBuilder.swift
//  vegapp
//
//  Created by diego garcia on 11/21/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

import Foundation

class QueryBuilder{
    private let VARIABLE_PATTERN = "?";
    private var query:String?
    private var params:[String]?
    
    init(){
        self.params = [String]()
    }
    
    func prepareStatement(query:String){
        self.query = query
        
    }
    
    func set(param:String){
        let value =  "'"+param+"'"
         self.params?.append(value)
    }
    
    func set(param:Int){
        self.params?.append(String(param))
    }
    
    func set(param:Double){
        self.params?.append(String(param))
    }
    
    func set(param:Bool){
        let value = "'"+String(param)+"'"
        self.params?.append(value)
    }
    
    func getQuery()-> String {
        var indexValue = 0
        while indexValue < (self.params?.count)! {
            query = query?.replacingOccurrences(of: VARIABLE_PATTERN, with: (self.params?[indexValue])!)
            indexValue += 1
        }
        self.params?.removeAll()
        return query ?? ""
    }
    
    
    
}
