    //
    //  DataBaseHelper.swift
    //  vegapp
    //
    //  Created by diego garcia on 10/31/18.
    //  Copyright © 2018 diego garcia. All rights reserved.
    //

    import Foundation
    import SQLite3


    class DataBaseHelper{
        
       var db: OpaquePointer? = nil
        var dbURL: URL? = nil
        
        
       public init(){
         dbURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("local.sqlite")
       }
        
        
        func openConnection() -> OpaquePointer{
            if sqlite3_open(dbURL!.path, &db) != SQLITE_OK {
                print("error opening database")
            }
            return db!
        }
        
        func closeConnection()  {
            sqlite3_close(db)
        }
        
        
    }
