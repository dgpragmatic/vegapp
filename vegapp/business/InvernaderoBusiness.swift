//
//  InvernaderoBusiness.swift
//  vegapp
//
//  Created by diego garcia on 11/14/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class InvernaderoBusiness{
    
    var invernaderoDAO:InvernaderoDAO
    private var conn:DataBaseHelper
    
    init(){
        conn = DataBaseHelper()
        invernaderoDAO = InvernaderoDAO(db:conn.openConnection())
    }
    
    func insertar(invernaderos:[InvernaderoDTO]){
        invernaderoDAO.clear(invernaderoDTO:invernaderos[0])
        for invernadero in invernaderos{
            invernaderoDAO.insert(invernaderoDTO: invernadero)
        }
        conn.closeConnection()
    }
    
    func findInvernaderoByFincaAndModulo(codFinca:String, codModulo:Int ) -> [InvernaderoDTO] {
        let invernaderos = invernaderoDAO.findInvernaderoByFincaAndModulo(codFinca:codFinca, codModulo:codModulo )
         conn.closeConnection()
        return invernaderos
    }
    
}
