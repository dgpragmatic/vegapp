//
//  TarjetaSiembraBusiness.swift
//  vegapp
//
//  Created by diego garcia on 11/21/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class TarjetaSiembraBusiness{
    
    private var tarjetaSiembraDAO:TarjetaSiembraDAO?
    private var conn:DataBaseHelper
    
    init(){
        conn = DataBaseHelper()
    }
    
    func insert(tarjetas:[TarjetaSiembraDTO]){
         tarjetaSiembraDAO = TarjetaSiembraDAO(db: conn.openConnection())
        if(tarjetas.count !=  0){
              tarjetaSiembraDAO?.clear(tarjetaSiembraDTO: tarjetas[0])
            for tarjetaSiembra in tarjetas {
                tarjetaSiembraDAO?.insert(tarjetaSiembraDTO: tarjetaSiembra)
            }
        }
        conn.closeConnection()
    }
    
    func findCamasByFincaAndBloqueAndModulo(codFinca:String, codBloque:String, codModulo:Int)->[CamaDTO]{
         tarjetaSiembraDAO = TarjetaSiembraDAO(db: conn.openConnection())
        let camas = tarjetaSiembraDAO?.findCamasByFincaAndBloqueAndModulo(codFinca: codFinca, codBloque: codBloque, codModulo: codModulo)
        conn.closeConnection()
       return camas!
       
    }
    
    func findFechaSincronizacionByFincaAndBloqueAndModulo(codFinca:String, codBloque:String, codModulo:Int)->String{
        tarjetaSiembraDAO = TarjetaSiembraDAO(db: conn.openConnection())
        let fechaSicronizacion = tarjetaSiembraDAO?.findFechaSincronizacionByFincaAndBloqueAndModulo(codFinca: codFinca, codBloque: codBloque, codModulo: codModulo)
        conn.closeConnection()
        return fechaSicronizacion!
    }
    
    func findCamaByCodFincaAndCodBloqueAndNroCamaAndModulo(codFinca:String,codBloque:String,nroCama:String,codModulo:Int)->[TarjetaSiembraDTO]{
        tarjetaSiembraDAO = TarjetaSiembraDAO(db: conn.openConnection())
        let tarjetas = tarjetaSiembraDAO!.findCamaByCodFincaAndCodBloqueAndNroCamaAndModulo(codFinca:codFinca,codBloque:codBloque,nroCama:nroCama,codModulo:codModulo)
        conn.closeConnection()
        return tarjetas
        
    }
    
    
}
