//
//  FincaBusiness.swift
//  vegapp
//
//  Created by diego garcia on 11/8/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

class FincaBusiness{
    
    private var fincaDAO:FincaDAO
    private var conn:DataBaseHelper
    
    init (){
        conn = DataBaseHelper()
        fincaDAO = FincaDAO(db:conn.openConnection())
    }
    
    func insert(fincas:[FincaDTO]){
        fincaDAO.clear()
        for finca in fincas {
            fincaDAO.insert(fincaDTO: finca)
        }
        conn.closeConnection()
    }
    
    func listAll() -> [FincaDTO] {
        let fincas = fincaDAO.listAll()
        conn.closeConnection()
        return fincas
    }
}
