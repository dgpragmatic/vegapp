//
//  SincronizeBusiness.swift
//  vegapp
//
//  Created by diego garcia on 10/31/18.
//  Copyright © 2018 diego garcia. All rights reserved.
//

import Foundation

 class SincronizeBusiness {
    
    var globalState:GlobalState?
    
    init() {
    }
    
    init(_ globalState:GlobalState) {
       self.globalState = globalState
    }
    
    public func getSincronizacionInicial(){
        let sincronizacionInicial = SincronizacionInicialClient(globalState!)
        sincronizacionInicial.startSincronizacion()
    }
    
    
    public func sincronizarTarjeta(invernaderos:[InvernaderoDTO]){
        let sincronizarTarjeta = SincronizacionTarjetaClient(self.globalState!)
        for invernadero in invernaderos {
            sincronizarTarjeta.startSincronizacion(invernaderoDTO: invernadero)
        }
        
    }
    
    public func sincronizarMestrosFincas
        (){
        let sincronizarMaestrosFinca = SincronizacionMaestroFincaClient(self.globalState!)
        sincronizarMaestrosFinca.startSincronizacion()
        
    }
    
    
}
